<?php

namespace App\Acme\Repository;

use App\ProgressReview;

class ProgressReviewRepository {


    /**
     * Returns all progress reviews within the accepted dates
     * @param $monthArray
     * @return array
     */
    public function allBetweenDates($monthArray)
    {
        $progressReviews = ProgressReview::whereBetween('created_at', [$monthArray[0], $monthArray[1]])->get();

        $children = [];

        foreach($progressReviews as $progressReview)
        {
            $child = $progressReview->child()->first();
            $outcome = $progressReview->outcome()->first();

            $children[] = $this->setCustomAttributesProgressReview($progressReview, $child, $outcome);
        }

        return $children;
    }

    public function allWithAtLeastOneOrange($monthArray)
    {
        // Get all of the progress reviews between the selected date
        $progressReviews = ProgressReview::whereBetween('created_at', [$monthArray[0], $monthArray[1]])->get();

        foreach($progressReviews as $progressReview)
        {
            // Find the child related to that review
            $child = $progressReview->child()->first();

            // The set of outcomes related to that review
            $outcome = $progressReview->outcome()->first();

            // Keep any that have been given one or more oranges
            if(in_array('Orange', $outcome['attributes']))
            {
                $children[] = $this->setCustomAttributesProgressReview($progressReview, $child, $outcome);
            }
        }

        return $children;
    }

    public function allWhereColumnIsOrange($monthArray, $outcomeColumn)
    {
        // Get all of the progress reviews between the selected date
        $progressReviews = ProgressReview::whereBetween('created_at', [$monthArray[0], $monthArray[1]])->get();

        //dd($outcomeColumn);

        foreach($progressReviews as $progressReview)
        {
            // Find the child related to that review
            $child = $progressReview->child()->first();

            // The set of outcomes related to that review
            $outcome = $progressReview->outcome()->first();

            // Keep any that have been given one or more oranges
            if($outcome->$outcomeColumn == 'Orange')
            {
                $children[] = $this->setCustomAttributesProgressReview($progressReview, $child, $outcome);
            }
        }

        return $children;
    }

    /**
     * helper function
     * @param $progressReview
     * @param $child
     * @param $outcome
     * @return mixed
     */
    public function setCustomAttributesProgressReview($progressReview, $child, $outcome)
    {
        $progressReview->setAttribute('full_name', $child->full_name);
        $progressReview->setAttribute('child_id', $child->id);
        $progressReview->setAttribute('outcome_id', $outcome->id);
        $progressReview->setAttribute('self_confidence', $outcome->self_confidence);
        $progressReview->setAttribute('managing_feelings', $outcome->managing_feelings);
        $progressReview->setAttribute('making_relationships', $outcome->making_relationships);
        $progressReview->setAttribute('listening_and_attention', $outcome->listening_and_attention);
        $progressReview->setAttribute('understanding', $outcome->understanding);
        $progressReview->setAttribute('speaking', $outcome->speaking);
        $progressReview->setAttribute('moving_and_handling', $outcome->moving_and_handling);
        $progressReview->setAttribute('health_self_care', $outcome->health_self_care);

        return $progressReview;
    }
}