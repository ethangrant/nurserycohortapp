<?php

namespace App\Acme\Repository;

use App\ProgressReview;

class CohortAnalysisRepository {

    public function countOrangesForEachOutcome($monthArray)
    {
        $self_confidence = 0;
        $making_relationships = 0;
        $managing_feelings = 0;
        $listening_and_attention = 0;
        $understanding = 0;
        $speaking = 0;
        $moving_and_handling = 0;
        $health_self_care = 0;


        $progressReviews = ProgressReview::whereBetween('created_at', [$monthArray[0], $monthArray[1]])->get();

        $outcomesArray = [];

        foreach($progressReviews as $progressReview)
        {
            $outcome = $progressReview->outcome()->select('self_confidence', 'making_relationships', 'managing_feelings', 'listening_and_attention', 'understanding', 'speaking', 'moving_and_handling', 'health_self_care')->get();
            $outcomesArray[] = $outcome;
        }

        foreach($outcomesArray as $outcome)
        {
            if($outcome[0]->self_confidence == 'Orange') {$self_confidence++;}
            if($outcome[0]->making_relationships == 'Orange') {$making_relationships++;}
            if($outcome[0]->managing_feelings == 'Orange') {$managing_feelings++;}
            if($outcome[0]->listening_and_attention == 'Orange') {$listening_and_attention++;}
            if($outcome[0]->understanding == 'Orange') {$understanding++;}
            if($outcome[0]->speaking == 'Orange') {$speaking++;}
            if($outcome[0]->moving_and_handling == 'Orange') {$moving_and_handling++;}
            if($outcome[0]->health_self_care == 'Orange') {$health_self_care++;}
        }

        $resultsArray =
            [
                ['Self Confidence', $self_confidence, '#2ecc71'],
                ['Making Relationships',$making_relationships , '#2ecc71'],
                ['Managing Feelings',$managing_feelings , '#2ecc71'],
                ['Listening and Attention',$listening_and_attention , '#2ecc71'],
                ['Understanding',$understanding , '#2ecc71'],
                ['Speaking',$speaking , '#2ecc71'],
                ['Moving and Handling',$moving_and_handling , '#2ecc71'],
                ['Health Self Care',$health_self_care , '#2ecc71'],
            ];

        return $resultsArray;
    }

}