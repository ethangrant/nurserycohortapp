<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RiskIndicator extends Model
{
    protected $fillable = [
        'born_between_aug_may',
        'sen',
        'child_in_care',
        'child_protection_plan',
        'EAL',
        'BME',
        'CAF',
        'two_year_funding',
        'joint_2y_review_with_health',
        'child_with_health_care_plan',
    ];

    public function child()
    {
        return $this->belongsTo(Child::class);
    }

    public function progressReviews()
    {
        return $this->hasMany(ProgressReview::class);
    }
}