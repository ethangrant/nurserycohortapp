<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Outcome extends Model
{
    protected $fillable = [
        'self_confidence',
        'making_relationships',
        'managing_feelings',
        'listening_and_attention',
        'understanding',
        'speaking',
        'moving_and_handling',
        'health_self_care',
    ];

    public function child()
    {
        return $this->belongsTo(Child::class);
    }

    public function progressReviews()
    {
        return $this->hasMany(ProgressReview::class);
    }
}