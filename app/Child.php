<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Child extends Model
{
    protected $fillable = [
        'full_name',
        'gender',
        'date_of_birth',
        'age_in_months'
    ];

    public function outcomes()
    {
        return $this->hasMany(Outcome::class);
    }

    public function riskIndicators()
    {
        return $this->hasMany(RiskIndicator::class);
    }

    public function riskOfDelays()
    {
        return $this->hasMany(RiskOfDelay::class);
    }

    public function progressReviews()
    {
        return $this->hasMany(ProgressReview::class);
    }
}
