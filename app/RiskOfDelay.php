<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RiskOfDelay extends Model
{
    protected $fillable = [
        'interaction_and_play',
        'attention_listening',
        'understanding2',
        'expressive_language',
        'speech_sounds',
    ];

    public function child()
    {
        return $this->belongsTo(Child::class);
    }

    public function progressReviews()
    {
        return $this->hasMany(ProgressReview::class);
    }
}