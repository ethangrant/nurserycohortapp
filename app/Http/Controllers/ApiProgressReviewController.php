<?php

namespace App\Http\Controllers;

use App\Acme\Repository;
use App\Child;
use App\Outcome;
use App\ProgressReview;
use App\RiskIndicator;
use App\RiskOfDelay;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\RiskIndicatorRequest;
use App\Http\Requests\OutcomeRequest;
use App\Http\Requests\RiskOfDelayRequest;
use App\Http\Controllers\ChromePhp;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class ApiProgressReviewController extends Controller
{
    public function index($monthRange, $year)
    {
        $monthArray = $this->getMonths($monthRange, $year);
        // Search database for progress reviews between these two dates
        $progressReviews = ProgressReview::whereBetween('created_at', [$monthArray[0], $monthArray[1]])->get();

        $children = [];

        foreach($progressReviews as $progressReview)
        {
            $child = $progressReview->child()->first();
            $progressReview->setAttribute('full_name', $child->full_name);
            $progressReview->setAttribute('child_id', $child->id);

            $children[] = $progressReview;
        }

        return response()->json($children);
    }

    public function getCohortOverview($monthRange, $year, $query, Repository\ProgressReviewRepository $progressReviewRepository)
    {
        $monthArray = $this->getMonths($monthRange, $year);

        switch($query)
        {
            case 'all':
                $children = $progressReviewRepository->allBetweenDates($monthArray);
                return response()->json($children);
            case 'all_with_orange':
                $children = $progressReviewRepository->allWithAtLeastOneOrange($monthArray);
                return response()->json($children);
            case 'self_confidence':
                $children = $progressReviewRepository->allWhereColumnIsOrange($monthArray, $query);
                return response()->json($children);
            case 'making_relationships':
                $children = $progressReviewRepository->allWhereColumnIsOrange($monthArray, $query);
                return response()->json($children);
            case 'managing_feelings':
                $children = $progressReviewRepository->allWhereColumnIsOrange($monthArray, $query);
                return response()->json($children);
            case 'listening_and_attention':
                $children = $progressReviewRepository->allWhereColumnIsOrange($monthArray, $query);
                return response()->json($children);
            case 'understanding':
                $children = $progressReviewRepository->allWhereColumnIsOrange($monthArray, $query);
                return response()->json($children);
            case 'speaking':
                $children = $progressReviewRepository->allWhereColumnIsOrange($monthArray, $query);
                return response()->json($children);
            case 'moving_and_handling':
                $children = $progressReviewRepository->allWhereColumnIsOrange($monthArray, $query);
                return response()->json($children);
            case 'health_self_care':
                $children = $progressReviewRepository->allWhereColumnIsOrange($monthArray, $query);
                return response()->json($children);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    public function store($id, RiskIndicatorRequest $riskIndicatorRequest, OutcomeRequest $outcomeRequest, RiskOfDelayRequest $riskOfDelayRequest)
    {
        //Collect the data
        $riskIndicator = $riskIndicatorRequest->all();
        $outcome = $outcomeRequest->all();
        $riskOfDelay = $riskOfDelayRequest->all();

        //Store in relevant tables
        $riskIndicator = RiskIndicator::create($riskIndicator);
        $outcome = Outcome::create($outcome);
        $riskOfDelay = RiskOfDelay::create($riskOfDelay);

        //Link back to the child
        $child = Child::find($id);

        $riskIndicator->child()->associate($child)->save();
        $outcome->child()->associate($child)->save();
        $riskOfDelay->child()->associate($child)->save();

        //Create the progress review
        $progressReview = ProgressReview::create();
        $progressReview->child()->associate($child)->save();
        $progressReview->riskIndicator()->associate($riskIndicator)->save();
        $progressReview->outcome()->associate($outcome)->save();
        $progressReview->riskOfDelay()->associate($riskOfDelay)->save();

        return 'pass';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $child = Child::find($id);

        $progressReviews = $child->progressReviews()->get();

        return response()->json($progressReviews);
    }

    public function view($id, $review)
    {
        $child = Child::find($id);
        $review = ProgressReview::find($review);

        $progressReview = [];

        $progressReview[] = $review->riskIndicator()->first();
        $progressReview[] = $review->outcome()->first();
        $progressReview[] = $review->riskOfDelay()->first();

        return response()->json($progressReview);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getMonths($monthRange, $year)
    {
        // Month array contains [month1, month2]
        $monthArray = explode('-', $monthRange);

        if(($monthArray[0] == 'november') && ($monthArray[1] == 'march'))
        {
            $month1 = new Carbon('first day of ' . $monthArray[0] . ' ' . ($year - 1));
            $month2 = new Carbon('first day of ' . $monthArray[1] . ' ' . $year);
        }
        else
        {
            // Get the first day of the two months provided
            $month1 = new Carbon('first day of ' . $monthArray[0] . ' ' . $year);
            $month2 = new Carbon('first day of ' . $monthArray[1] . ' ' . $year);
        }
        unset($monthArray);
        $monthArray[] = $month1;
        $monthArray[] = $month2;

        return $monthArray;
    }
}
