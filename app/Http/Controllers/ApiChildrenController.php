<?php

namespace App\Http\Controllers;

use App\Child;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class ApiChildrenController extends Controller
{
    /**
     * Returning a list of children from the database.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Child::all());
    }

    public function store(Requests\ChildRequest $childRequest)
    {
        //Get the input data
        $child = $childRequest->all();

        //Calculate age in months
        $dob = new \DateTime($child['date_of_birth']);
        $months = $this->ageMonths($dob);

        $child['age_in_months'] = $months;
        $child['date_of_birth'] = $dob;

        //Store data in model
        $child = Child::create($child);

        return 'Child created';
    }

    /**
     * Retrieve child data ready for editing
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        return response()->json(Child::find($id));
    }

    /**
     * Updates child details
     * @param $id
     */
    public function update($id, Requests\ChildRequest $childRequest)
    {
        $child = Child::find($id);
        $child->full_name = $childRequest->get('full_name');
        $child->gender = $childRequest->get('gender');

        //Calculate age in months
        $dob = new \DateTime($childRequest->get('date_of_birth'));
        $months = $this->ageMonths($dob);

        $child->age_in_months = $months;
        $child->date_of_birth = $dob;

        $child->save();
    }

    /**
     * Finds a record similar to that of the users query
     * and returns it as a JSON response for angular.
     * @param $searchQuery
     * @return \Illuminate\Http\JsonResponse
     */
    public function search($searchQuery)
    {
        return response()->json(Child::where('full_name', 'LIKE', '%'.$searchQuery.'%')->get());
    }

    /**
     * Deletes a Child record
     * @param $id
     * @return string
     */
    public function destroy($id)
    {
        Child::find($id)->delete();

        return 'Child removed';
    }

    public function ageMonths(\DateTime $dob)
    {
        $diff = $dob->diff(new \DateTime());
        $months = $diff->format('%m') + 12 * $diff->format('%y');

        return $months;
    }
}
