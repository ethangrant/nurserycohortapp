<?php

namespace App\Http\Controllers;

use App\Child;
use Faker\Provider\DateTime;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class ChildrenController extends Controller
{
    /**
     * Returning a list of children from the database.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('children.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('children.create');
    }

    /**
     * Saves new child to database, before it can be stores
     * childs age in months must be calculated.
     * @param Requests\ChildRequest $childRequest
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Requests\ChildRequest $childRequest)
    {
        //Get the input data
        $child = $childRequest->all();

        //Calculate age in months
        $dob = new \DateTime($child['date_of_birth']);
        $months = $this->ageMonths($dob);

        $child['age_in_months'] = $months;
        $child['date_of_birth'] = $dob;

        //Store data in model
        $child = Child::create($child);

        return redirect()->action('ChildrenController@index')->with('success', 'Child added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $child = Child::where('id', '=', $id)->first();

        return view('children.show', compact('child'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function update(Requests\ChildRequest $childRequest, $id)
    {
        //Get the input data
        $data = $childRequest->all();

        $child = Child::find($id);

        $child = $child->save($data);

        return $child;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function ageMonths(\DateTime $dob)
    {
        $diff = $dob->diff(new \DateTime());
        $months = $diff->format('%m') + 12 * $diff->format('%y');

        return $months;
    }
}
