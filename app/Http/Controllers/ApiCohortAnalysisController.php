<?php

namespace App\Http\Controllers;

use App\Acme\Repository\CohortAnalysisRepository;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class ApiCohortAnalysisController extends Controller
{
    public function mainChart(CohortAnalysisRepository $cohortAnalysisRepository)
    {
        // Get all progress reviews between november of last year to november of this year
        $monthArray = $this->getMonths('november-november', Date('Y'));

        $monthArray[0] = $monthArray[0]->subYears(1);

        $chart = $cohortAnalysisRepository->countOrangesForEachOutcome($monthArray);

        return response()->json($chart);
    }

    public function getMonths($monthRange, $year)
    {
        // Month array contains [month1, month2]
        $monthArray = explode('-', $monthRange);

        if(($monthArray[0] == 'november') && ($monthArray[1] == 'march'))
        {
            $month1 = new Carbon('first day of ' . $monthArray[0] . ' ' . ($year - 1));
            $month2 = new Carbon('first day of ' . $monthArray[1] . ' ' . $year);
        }
        else
        {
            // Get the first day of the two months provided
            $month1 = new Carbon('first day of ' . $monthArray[0] . ' ' . $year);
            $month2 = new Carbon('first day of ' . $monthArray[1] . ' ' . $year);
        }
        unset($monthArray);
        $monthArray[] = $month1;
        $monthArray[] = $month2;

        return $monthArray;
    }
}
