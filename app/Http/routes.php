<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
 * Progress review Routes
 */
Route::get('api/progressReview/show/{id}', 'ApiProgressReviewController@show');
Route::get('api/progressReview/{monthRange}/{year}', 'ApiProgressReviewController@index');
Route::post('api/progressReview/store/{id}', 'ApiProgressReviewController@store');
Route::get('api/progressReview/view/{id}/{review}', 'ApiProgressReviewController@view');
Route::get('api/progressReview/cohortOverview/{monthRange}/{year}/{query}', 'ApiProgressReviewController@getCohortOverview');

/**
 * Cohort analysis routes
 */
Route::get('api/cohortAnalysis', 'ApiCohortAnalysisController@mainChart');

/**
 * Children routes
 */
Route::get('api/children', 'ApiChildrenController@index');
Route::get('api/children/edit/{id}', 'ApiChildrenController@edit');
Route::get('api/children/search/{full_name}', 'ApiChildrenController@search');
Route::post('api/children/store', 'ApiChildrenController@store');
Route::put('api/children/{id}', 'ApiChildrenController@update');
Route::delete('api/children/{id}', 'ApiChildrenController@destroy');

/*Route::any('{path?}', function()
{
    return view("pages/index");
})->where("path", ".+");*/

/**
 * Authentication
 */
Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);