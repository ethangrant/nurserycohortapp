<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RiskIndicatorRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'born_between_aug_may' => 'required',
            'sen' => 'required',
            'child_in_care' => 'required',
            'child_protection_plan' => 'required',
            'EAL' => 'required',
            'BME' => 'required',
            'CAF' => 'required',
            'two_year_funding' => 'required',
            'joint_2y_review_with_health' => 'required',
            'child_with_health_care_plan' => 'required',
        ];
    }
}
