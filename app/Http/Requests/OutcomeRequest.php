<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OutcomeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'self_confidence' => 'required',
            'making_relationships' => 'required',
            'managing_feelings' => 'required',
            'listening_and_attention' => 'required',
            'understanding' => 'required',
            'speaking' => 'required',
            'moving_and_handling' => 'required',
            'health_self_care' => 'required',
        ];
    }
}
