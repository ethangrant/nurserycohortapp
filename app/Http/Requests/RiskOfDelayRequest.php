<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RiskOfDelayRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'interaction_and_play' => 'required',
            'attention_listening' => 'required',
            'understanding2' => 'required',
            'expressive_language' => 'required',
            'speech_sounds' => 'required',
        ];
    }
}
