<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgressReview extends Model
{
    protected $fillable = [
        'child_id',
        'risk_indicator_id',
        'outcome_id',
        'risk_of_delay_id',
    ];

    public function child()
    {
        return $this->belongsTo(Child::class);
    }

    public function riskIndicator()
    {
        return $this->belongsTo(RiskIndicator::class);
    }

    public function outcome()
    {
        return $this->belongsTo(Outcome::class);
    }

    public function riskOfDelay()
    {
        return $this->belongsTo(RiskOfDelay::class);
    }

    /**
     * Query scopes below
     */

    /**
     * scopeOrange returns all the children that have been given one or more
     * oranges in the outcomes section of the review.
     */

}
