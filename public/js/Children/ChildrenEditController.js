
app.controller('ChildrenEditController', ['$scope', '$http', '$timeout', '$stateParams', 'ChildrenService', 'ProgressReviewService', function($scope, $http, $timeout, $stateParams, ChildrenService, ProgressReviewService) {

    // Initially grabs the data of the child from the id passed in the url
    $http.get('api/children/edit/' + $stateParams.id).success(function(data) {$scope.child = data;});

    ProgressReviewService.childProgressReview($stateParams.id).then(function(response) {
        $scope.reviews = response.data;
    });

    $scope.update = function(id, full_name, gender, age_in_months, dob) {
        ChildrenService.update(id, full_name, gender, age_in_months, dob).then(function(response) {
            $scope.outcome = response;
            if($scope.outcome == 'success'){
                $scope.success = true;
                $scope.msg = 'Child successfully updated!';
                $timeout(function(){
                    $scope.success = false;
                }, 5000);
            } else if($scope.outcome == 'fail') {
                $scope.fail = true;
                $scope.msg = 'Fail!';
                $timeout(function(){
                    $scope.fail = false;
                }, 5000);
            }
        });
    }

}]);