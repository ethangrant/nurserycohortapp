/**
 * Child search controller handles the page search for a specific
 * child but also has functionality to delete child records.
 */
app.controller('ChildrenController', ['$scope', '$http', '$timeout', 'ChildrenService', function($scope, $http, $timeout, ChildrenService) {

    // Initially get all child records available an return them to the view.
    $http.get('api/children').success(function(data) {
        $scope.children = data;
        for(var count = 0; count < $scope.children.length; ++count)
        {
            $scope.children[count].date_of_birth = new Date($scope.children[count].date_of_birth);
        }
    });

    /**
     * Function activates as user begins to type, retrieving
     * real time results from the child model.
     * @param searchQuery
     * @returns {*}
     */
    $scope.search = function(searchQuery) {
        ChildrenService.searchChildren(searchQuery).then(function(response){
            $scope.children = response;
            for(var count = 0; count < $scope.children.length; ++count)
            {
                $scope.children[count].date_of_birth = new Date($scope.children[count].date_of_birth);
            }
        });
    }

    /**
     * Deletes a selected record from children table
     * @param id
     * @returns {*}
     */
    $scope.delete = function(id) {
        ChildrenService.delete(id).then(function(response) {
            $scope.children = response;
            $scope.success = true;
            $scope.msg = 'Child successfully removed!';
            $timeout(function(){
                $scope.success = false;
            }, 5000);
        });
    }

    /**
     * Allows user to create a new child
     */
    $scope.create = function(full_name, gender, dob) {
        ChildrenService.create(full_name, gender, dob).then(function(response) {
            $scope.success = response;
            $scope.msg = 'Child added successfully!';
            $timeout(function(){
                $scope.success = false;
            }, 5000);
        });
    }
}]);