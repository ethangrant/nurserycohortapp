app.service('ChildrenService', ['$http', '$timeout', function($http, $timeout) {

    this.searchChildren = function(searchQuery) {

        if(searchQuery !== '') {
            var promise = $http.get('api/children/search/' + searchQuery).then(function (response) {return response.data;});
        } else {
            var promise = $http.get('api/children').then(function(response) {return response.data;});
        }

        return promise;
    }

    this.delete = function(id) {
        console.log(id);
        $http.delete('api/children/' + id);

        var promise = $http.get('api/children').then(function(response) {return response.data;});

        return promise;
    }

    this.create = function(full_name, gender, dob) {
        var promise = $http.post('/api/children/store', {full_name:full_name, gender:gender, date_of_birth:dob})
            .then(function(response) {
                var success = true;
                return success;
            });
        return promise;
    }

    this.update = function(id, full_name, gender, age_in_months, dob) {

        var promise = $http.put('/api/children/' + id, {full_name:full_name, gender:gender, age_in_months:age_in_months, date_of_birth:dob})
            .then(function(response) {
                var outcome = 'success';
                return outcome;
            }, function(response) {
                var outcome = 'fail';
                return outcome;
            });

        return promise;
    }

}]);