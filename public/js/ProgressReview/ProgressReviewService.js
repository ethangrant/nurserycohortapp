app.service('ProgressReviewService', ['$http', '$timeout', function($http, $timeout) {

    this.create = function(id, born_between_aug_may, sen, child_in_care, child_protection_plan, EAL, BME, CAF, two_year_funding, joint_2y_review_with_health,
                           child_with_health_care_plan, self_confidence, making_relationships, managing_feelings, listening_and_attention,
                           understanding, speaking, moving_and_handling, health_self_care, interaction_and_play,
                           attention_listening, understanding2, expressive_language, speech_sounds) {

        var promise = $http.post('api/progressReview/store/' + id, {born_between_aug_may:born_between_aug_may, sen:sen, child_in_care:child_in_care, child_protection_plan:child_protection_plan,
            EAL:EAL, BME:BME, CAF:CAF, two_year_funding:two_year_funding, joint_2y_review_with_health:joint_2y_review_with_health, child_with_health_care_plan:child_with_health_care_plan,
            self_confidence:self_confidence, making_relationships:making_relationships, managing_feelings:managing_feelings, listening_and_attention:listening_and_attention,
            understanding:understanding, speaking:speaking, moving_and_handling:moving_and_handling, health_self_care:health_self_care, interaction_and_play:interaction_and_play,
            attention_listening:attention_listening, understanding2:understanding2, expressive_language:expressive_language, speech_sounds:speech_sounds})
            .then(function(response) {
                //success
                var success = true;
                return success;
            }, function(response) {
                //fail
            });
        return promise;
    }

    this.childProgressReview = function(id) {
        var promise = $http.get('api/progressReview/show/' + id).success(function(response) {
            return response;
        })
        return promise;
    }

    this.viewProgressReview = function(id, review) {
        var promise = $http.get('api/progressReview/view/' + id + '/' + review).success(function(response) {
            return response;
        })
        return promise;
    }

    this.getProgressReviews = function(monthRange, year) {
        var promise = $http.get('api/progressReview/' + monthRange + '/' + year).success(function(response) {
            return response;
        })
        return promise;
    }

    this.getCohortOverview = function(monthRange, year, query) {
        var promise = $http.get('api/progressReview/cohortOverview/' + monthRange + '/' + year + '/' + query).success(function(response) {
            return response;
        })
        return promise;
    }

}]);