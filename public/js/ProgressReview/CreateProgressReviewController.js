
app.controller('CreateProgressReviewController', ['$scope', '$http', '$timeout', '$stateParams', 'ProgressReviewService', function($scope, $http, $timeout, $stateParams, ProgressReviewService) {

    // Initially grabs the data of the child from the id passed in the url
    $http.get('api/children/edit/' + $stateParams.id).success(function(data) {$scope.child = data; console.log($scope.child);});

    $scope.create = function(id, born_between_aug_may, sen, child_in_care, child_protection_plan, EAL, BME, CAF, two_year_funding,
                             joint_2y_review_with_health, child_with_health_care_plan, self_confidence, making_relationships, managing_feelings,
                             listening_and_attention, understanding, speaking, moving_and_handling, health_self_care, interaction_and_play,
                             attention_listening, understanding2, expressive_language, speech_sounds) {

        console.log(id, born_between_aug_may, sen, child_in_care, child_protection_plan, EAL, BME, CAF, two_year_funding,
            joint_2y_review_with_health, child_with_health_care_plan, self_confidence, making_relationships, managing_feelings,
            listening_and_attention, understanding, speaking, moving_and_handling, health_self_care, interaction_and_play,
            attention_listening, understanding2, expressive_language, speech_sounds);

        ProgressReviewService.create(
            id,
            born_between_aug_may,
            sen,
            child_in_care,
            child_protection_plan,
            EAL,
            BME,
            CAF,
            two_year_funding,
            joint_2y_review_with_health,
            child_with_health_care_plan,
            self_confidence,
            making_relationships,
            managing_feelings,
            listening_and_attention,
            understanding,
            speaking,
            moving_and_handling,
            health_self_care,
            interaction_and_play,
            attention_listening,
            understanding2,
            expressive_language,
            speech_sounds)

            .then(function(response) {
            $scope.success = response;
            $scope.msg = 'Progress Review added successfully!';
            $timeout(function(){
                $scope.success = false;
            }, 5000);
        });

    }

}]);