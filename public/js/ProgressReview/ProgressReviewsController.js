
app.controller('ProgressReviewsController', ['$scope', '$http', '$timeout', '$stateParams', 'ProgressReviewService', function($scope, $http, $timeout, $stateParams, ProgressReviewService) {

    $scope.getProgressReviews = function(monthRange, year) {
        ProgressReviewService.getProgressReviews(monthRange, year).then(function(response) {
            $scope.reviews = response.data;
            for(var count = 0; count < $scope.reviews.length; ++count)
            {
                $scope.reviews[count].created_at = new Date($scope.reviews[count].created_at);
            }
        })
    }

    $scope.getCohortOverview = function(monthRange, year, query) {
        ProgressReviewService.getCohortOverview(monthRange, year, query).then(function(response) {
            $scope.reviews = response.data;
            for(var count = 0; count < $scope.reviews.length; ++count)
            {
                $scope.reviews[count].created_at = new Date($scope.reviews[count].created_at);
            }
            console.log($scope.reviews[0].created_at);
        })
    }

}]);