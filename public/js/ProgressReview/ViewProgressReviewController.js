
app.controller('ViewProgressReviewController', ['$scope', '$http', '$timeout', '$stateParams', 'ProgressReviewService', function($scope, $http, $timeout, $stateParams, ProgressReviewService) {

    // Initially grabs the data of the child from the id passed in the url
    $http.get('api/children/edit/' + $stateParams.id).success(function(data) {$scope.child = data; console.log($scope.child);});

    ProgressReviewService.viewProgressReview($stateParams.id, $stateParams.review).then(function(response) {
        $scope.riskIndicators = response.data[0];
        $scope.outcomes = response.data[1];
        $scope.riskOfDelays = response.data[2];
    });

}]);