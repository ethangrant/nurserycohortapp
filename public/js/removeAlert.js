/**
 * On page load if an alert message exists on the page
 * the below function will remove after x seconds.
 */

    if($('.alert-danger').length > 0) {
        window.setInterval(function(){
            $('.alert-danger').addClass('bounceOutDown');
        }, 5000);
    }

    if($('.alert-success').length > 0) {
        window.setInterval(function(){
            $('.alert-success').addClass('bounceOutDown');
        }, 3000);
    }