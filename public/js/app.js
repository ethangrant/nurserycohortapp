var app = angular.module('app', ['ui.router', 'ngAnimate', 'chart.js']);

    app.config(['$urlRouterProvider', '$stateProvider', 'ChartJsProvider', function($urlRouterProvider, $stateProvider, ChartJsProvider) {
        $urlRouterProvider.otherwise('/');
        $stateProvider
            .state('index', {
                url: '/index',
                templateUrl: 'index.html'
            })
            .state('children', {
                url: '/children',
                templateUrl: 'partials/children.html',
                controller: 'ChildrenController'

            })
            .state('createChild', {
                url: '/children/create',
                templateUrl: 'partials/createChild.html',
                controller: 'ChildrenController',
            })
            .state('editChild', {
                url: '/children/edit/:id',
                templateUrl: 'partials/editChild.html',
                controller: 'ChildrenEditController',
            })
            .state('createProgressReview', {
                url: '/children/create/progressReview/:id',
                templateUrl: 'partials/createProgressReview.html',
                controller: 'CreateProgressReviewController',
            })
            .state('viewProgressReview', {
                url: '/children/view/progressReview/:id/:review',
                templateUrl: 'partials/viewProgressReview.html',
                controller: 'ViewProgressReviewController',
            })
            .state('progressReviews', {
                url: '/progressReviews',
                templateUrl: 'partials/progressReviews.html',
                controller: 'ProgressReviewsController',
            })
            .state('cohortOverview', {
                url: '/outcomeOverview',
                templateUrl: 'partials/cohortOverview.html',
                controller: 'ProgressReviewsController',
            })
            .state('CohortAnalysis', {
                url: '/cohortAnalysis',
                templateUrl: 'partials/cohortAnalysis.html',
                controller: 'CohortAnalysisController',
            })
            .state('about', {
                url: '/about',
                templateUrl: 'partials/about.html',
            })
    }])