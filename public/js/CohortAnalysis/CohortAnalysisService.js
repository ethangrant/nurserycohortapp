app.service('CohortAnalysisService', ['$http', '$timeout', function($http, $timeout) {

    this.mainChart = function() {
        var promise = $http.get('api/cohortAnalysis').then(function(response) {
            return response;
        })
        return promise;
    }

}]);