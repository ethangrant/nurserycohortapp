@if (session('success'))
    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-success animated bounceInUp">
                <div class="row">
                    <div class="col-xs-1">
                        <img class="successImg" src="/img/success64.png" alt="Success">
                    </div>
                    <div class="col-xs-11">
                        <div class="">
                            {{ session('success') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif