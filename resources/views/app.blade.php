<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Nursery Cohort Progress Overview</title>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/all.css">
    <script></script>
</head>
<body>

    @if(Auth::guest())
        @include('partials.nav')
        <div class="container">
            @yield('content')
        </div>
    @endif

    @if(Auth::user())
        @include('partials.nav')
        <div class="container" ng-app="app">
            <div ui-view></div>
        </div>
    @endif

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.7/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.2.15/angular-ui-router.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/flat-ui.min.js"></script>
    <script src="/js/bootstrap-datepicker.min.js"></script>
    <script src="/js/datePicker.js"></script>
    <script src="/js/radioButtons.js"></script>
    <script src="/js/childSearch.js"></script>
    <script src="/js/removeAlert.js"></script>

</body>
</html>