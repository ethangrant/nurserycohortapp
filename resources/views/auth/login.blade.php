@extends('app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div id="loginPanel" class="panel panel-default">
                    <div class="panel-heading">Login</div>
                    <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
{{--                                <div class="col-sm-offset-3 col-sm-6 col-sm-offset-3 tcenter">
                                    <label class="control-label">E-Mail Address</label>
                                </div>--}}
                                <div class="col-sm-offset-3 col-sm-6 col-sm-offset-3">
                                    <input type="email" class="form-control" placeholder="E-Mail Address" name="email" value="{{ old('email') }}">
                                </div>
                            </div>

                            <div class="form-group">
{{--                                <div class="col-sm-offset-3 col-sm-6 col-sm-offset-3 tcenter">
                                    <label class="col-md-4 control-label">Password</label>
                                </div>--}}
                                <div class="col-sm-offset-3 col-sm-6 col-sm-offset-3">
                                    <input type="password" class="form-control" placeholder="Password" name="password">
                                </div>
                            </div>

{{--                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="remember"> Remember Me
                                        </label>
                                    </div>
                                </div>
                            </div>--}}

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" class="btn btn-embossed submitButton center-block">Login</button>
                                </div>
                            </div>

                            <div class="">
                                <div class="col-sm-12 tcenter">
                                    <a class="btn btn-link tcenter" href="{{ url('/password/email') }}">Forgot Your Password?</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection