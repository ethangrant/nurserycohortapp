@extends('app')

@section('content')

    <div class="row">
        <div class="col-sm-12">
            <h3 class="tcenter">Progress review for - {{ $child->full_name }}</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <h4>Risk Indicators</h4>
            <table class="table table-striped table-bordered table-responsive">
                <tr>
                    <th>Born May-Aug</th>
                    <th>SEN: EYA/+ SA/+</th>
                    <th>Child in care</th>
                    <th>Child protection plan</th>
                    <th>EAL</th>
                    <th>BME</th>
                    <th>CAF</th>
                    <th>Eligible for 2 year funding</th>
                    <th>Joint 2y o review with health</th>
                    <th>Child with health care plan</th>
                </tr>
                <tr>
                    {!! Form::open() !!}
                    <td>
                        <span>
                            <label class="radio" for="radio1">
                                <input type="radio" name="optionsRadios1" value="Yes" id="radio1" data-toggle="radio" class="custom-radio"><span class="icons"><span class="icon-unchecked"></span><span class="icon-checked"></span></span>
                                Yes
                            </label>
                        </span>
                        <span>
                            <label class="radio" for="radio2">
                                <input type="radio" name="optionsRadios1" value="No" id="radio2" data-toggle="radio" class="custom-radio"><span class="icons"><span class="icon-unchecked"></span><span class="icon-checked"></span></span>
                                No
                            </label>
                        </span>
                    </td>
                    {!! Form::close() !!}
                </tr>
            </table>
        </div>
    </div>

@endsection