@extends('app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h1 class="tcenter">Add new child</h1>
        </div>
    </div>

    <div class="row">

        <div class="row">
            <div class="col-sm-6">

                {!! Form::open(['action' => 'ChildrenController@store']) !!}

                    <div class="form-group">
                        {!! Form::text('full_name', null, ['class' => 'form-control flat', 'placeholder' => 'Full Name']) !!}
                    </div>

                    <div class="form-group">
                        <select name="gender" class="form-control flat">
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                    </div>

                    <div class="input-group date">
                        <input name="date_of_birth" type="text" class="form-control datePickerInput" placeholder="Date of Birth"><span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
                    </div>

                    <div class="form-group">
                        <button type="button" class="btn btn-embossed uploadImageButton center-block">Upload Profile Image</button>
                    </div>

                    <div class="form-group">
                        {!! Form::submit('Submit', ['class' => 'btn btn-embossed btn-success submitFormButton center-block']) !!}
                    </div>

                {!! Form::close() !!}

            </div>

            <div class="col-sm-6">
                <img class="img-circle img-responsive" src="/img/person-placeholder.jpg" alt="child profile image">
            </div>
        </div>

    </div>

    @include('errors.errorlist')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="/js/removeAlert.js"></script>

@endsection