@extends('app')

@section('content')

    <div ng-app="app" ng-controller="ChildSearchController"> <!-- Angular controller hook -->
        <div class="row">
            <div class="col-sm-12">
                <h1 class="tcenter">Active Children</h1>
            </div>
        </div>

        <div class="search">
            <div class="row">
                <div class="col-sm-offset-4 col-sm-4">
                    <form action="children" method="GET">
                        <div class="form-group">
                            <input ng-change="search(searchQuery)" type="text" class="form-control flat" ng-model="searchQuery" placeholder="Search">
                            <span class="glyphicon glyphicon-search"></span>
                        </div>
                    </form>
                </div>
                <div class="col-sm-4">
                    <a href="{{ url('children/create') }}" type="button" class="btn btn-embossed btn-success">New Child  <span class="glyphicon glyphicon-plus"></span></a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <table class="table table-striped table-responsive">
                    <tr>
                        <th>Name</th>
                        <th>Gender</th>
                        <th>Date of Birth</th>
                        <th>Age in Months</th>
                        <th>Actions</th>
                    </tr>
                    <tr ng-repeat="child in children">
                        <td>@{{ child.full_name }}</td>
                        <td>@{{ child.gender }}</td>
                        <td>@{{ child.date_of_birth }}</td>
                        <td>@{{ child.age_in_months }}</td>
                        <td>
                            <a href="@{{ 'children/' + child.id }} " type="button" class="btn btn-embossed viewProfileButton">View</a>
                            <a href="@{{ 'progressReview/create/' + child.id }} " type="button" class="btn btn-embossed progressReviewButton">Progress Review</a>
                            <button ng-Click="delete(child.id)" type="button" class="btn btn-embossed btn-danger">X</button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    @include('partials.successMsg')

@endsection