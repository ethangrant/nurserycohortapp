@if ($errors->any())
    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-danger animated bounceInUp">
                <div class="col-xs-1">
                    <img src="/img/fail64.png" alt="fail">
                </div>
                <div class="col-xs-11">
                    <div class="">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        </div>
@endif