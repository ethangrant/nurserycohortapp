<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgressReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('progress_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('child_id')->unsigned()->nullable();
            $table->integer('risk_indicator_id')->unsigned()->nullable();
            $table->integer('outcome_id')->unsigned()->nullable();
            $table->integer('risk_of_delay_id')->unsigned()->nullable();

            $table->foreign('child_id')->references('id')->on('children')->onDelete('cascade');
            $table->foreign('risk_indicator_id')->references('id')->on('risk_indicators')->onDelete('cascade');
            $table->foreign('outcome_id')->references('id')->on('outcomes')->onDelete('cascade');
            $table->foreign('risk_of_delay_id')->references('id')->on('risk_of_delays')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('progress_reviews');
    }
}
