<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRiskOfDelaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('risk_of_delays', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('child_id')->unsigned()->nullable();
            $table->string('interaction_and_play');
            $table->string('attention_listening');
            $table->string('understanding2');
            $table->string('expressive_language');
            $table->string('speech_sounds');
            $table->timestamps();

            $table->foreign('child_id')->references('id')->on('children')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('risk_of_delays');
    }
}
