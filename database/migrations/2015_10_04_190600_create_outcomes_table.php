<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOutcomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outcomes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('child_id')->unsigned()->nullable();
            $table->string('self_confidence');
            $table->string('making_relationships');
            $table->string('managing_feelings');
            $table->string('listening_and_attention');
            $table->string('understanding');
            $table->string('speaking');
            $table->string('moving_and_handling');
            $table->string('health_self_care');
            $table->timestamps();

            $table->foreign('child_id')->references('id')->on('children')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('outcomes');
    }
}
