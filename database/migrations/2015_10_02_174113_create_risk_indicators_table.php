<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRiskIndicatorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('risk_indicators', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('child_id')->unsigned()->nullable();
            $table->string('born_between_aug_may');
            $table->string('sen');
            $table->string('child_in_care');
            $table->string('child_protection_plan');
            $table->string('EAL');
            $table->string('BME');
            $table->string('CAF');
            $table->string('two_year_funding');
            $table->string('joint_2y_review_with_health');
            $table->string('child_with_health_care_plan');
            $table->timestamps();

            $table->foreign('child_id')->references('id')->on('children')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('risk_indicators');
    }
}
